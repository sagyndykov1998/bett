from bs4 import BeautifulSoup
import requests


def calc():
    r = requests.get("https://jetlend.ru/borrower/")
    html = r.text
    soup = BeautifulSoup(html, "html.parser")
    c = 0
    for tag in soup.find_all():
        if tag.attrs:
            c = c + 1
    return len(soup.find_all()), c


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    calc()

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
