from typing import List

from fastapi import APIRouter, Depends
from sqlalchemy.ext.asyncio import AsyncSession
from starlette import status

from api.v1.depends import get_session
from schemas.bet_schemas import BetOut, BetIn, Bet
from schemas.enums import BetStatus
from usecases.bet_usecases import create_bet_usecase, list_all_bet_usecase

router = APIRouter()


@router.post(
    '/',
    response_model=BetOut,
    status_code=status.HTTP_200_OK,
)
async def create_bet(
        new_bet: BetIn,
        db_session: AsyncSession = Depends(get_session)
):
    new_bet = Bet(**new_bet.dict(), status=BetStatus.not_played)
    return await create_bet_usecase(db_session, new_bet)


@router.get(
    '/',
    response_model=List[BetOut],
    status_code=status.HTTP_200_OK,
)
async def get_bets(
        db_session: AsyncSession = Depends(get_session)
):
    return await list_all_bet_usecase(db_session)
