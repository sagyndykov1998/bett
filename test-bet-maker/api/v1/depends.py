from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm import Session

from api.pagination import Pagination
from resource_access.db_session import AsyncSessionLocal
from resource_access.db_session import SessionLocal


async def get_session() -> AsyncSession:
    async with AsyncSessionLocal() as session:
        yield session


async def get_sync_session() -> Session:
    with SessionLocal() as session:
        yield session


def get_db():
    db = SessionLocal()
    yield db


async def get_pagination(offset: int = 0, limit: int = 20) -> Pagination:
    return Pagination(offset=offset, limit=limit)
