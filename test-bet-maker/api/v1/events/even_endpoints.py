from fastapi import APIRouter, Depends
from sqlalchemy.ext.asyncio import AsyncSession
from starlette import status

from api.v1.depends import get_session
from schemas.event_schemas import Event
from usecases.event_usecases import get_all_events_usecase, react_to_event_status_usecase

router = APIRouter()


@router.get(
    '/',
    status_code=status.HTTP_200_OK,
)
async def get_events():
    return await get_all_events_usecase()


@router.post(
    '/{event_id}/state/{state}',
    status_code=status.HTTP_200_OK,
)
async def react_to_event_status_changes(
        state: str,
        event_id: str,
        db_session: AsyncSession = Depends(get_session)
):
    event_with_new_status = Event(event_id=event_id, state=state)
    return await react_to_event_status_usecase(db_session, event_with_new_status)
