from fastapi import APIRouter
from .bets import bet_endpoints
from .events import even_endpoints

v1_router = APIRouter()

v1_router.include_router(bet_endpoints.router, prefix="/bets")
v1_router.include_router(even_endpoints.router, prefix="/events")
