from typing import List

from pydantic import parse_obj_as
from sqlalchemy import select, update
from sqlalchemy.ext.asyncio import AsyncSession

from resource_access.models.bets import BetDB
from schemas.bet_schemas import Bet
from schemas.enums import BetStatus
from schemas.event_schemas import Event


class BetRepositories:
    def __init__(self, session: AsyncSession):
        self.session = session
        self.model = BetDB

    async def create_bet(self, new_bet: Bet) -> Bet:
        new_bet = BetDB(**new_bet.dict())
        self.session.add(new_bet)
        await self.session.commit()
        await self.session.refresh(new_bet)
        return Bet.from_orm(new_bet)

    async def get_bets(self) -> List[Bet]:
        query = await self.session.execute(
            select(self.model)
        )
        query_data = query.scalars().all()
        return parse_obj_as(List[Bet], query_data)

    async def update_not_played_bets_statuses_by_event_id(self, event_id: str, status: BetStatus):
        stmt = (
            update(self.model)
                .where(self.model.event_id == event_id, self.model.status == BetStatus.not_played)
                .values(status=status)
        )
        await self.session.execute(stmt)
        await self.session.commit()
