from sqlalchemy import Column, Integer
from sqlalchemy.ext.declarative import declarative_base


class Base:
    # Generate id automatically
    id = Column(Integer, primary_key=True, index=True)


Base = declarative_base(cls=Base)
