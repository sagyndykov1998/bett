from sqlalchemy.ext.asyncio import create_async_engine, AsyncSession
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, Session

from core.config import settings

async_engine = create_async_engine(settings.get_async_database_url())
AsyncSessionLocal = sessionmaker(bind=async_engine, expire_on_commit=False,
                                 class_=AsyncSession)

sync_engine = create_engine(settings.get_database_url())
SessionLocal = sessionmaker(bind=sync_engine, expire_on_commit=False,
                            class_=Session)
