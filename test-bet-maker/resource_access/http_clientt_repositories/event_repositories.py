from typing import List

import httpx
from starlette import status

from schemas.event_schemas import Event
from utils.common_exceptions import EventServiceNotAvailableException, NotFoundException


class EventRepositories:
    def __init__(self):
        self.client = httpx.AsyncClient()

    async def get_all_events(self) -> List[Event]:
        response = await self.client.get("http://line-provider-fast-api:8080/events")  # Url to constant
        if response.status_code == status.HTTP_200_OK:
            return response.json()
        raise EventServiceNotAvailableException()

    async def get_event(self, event_id) -> Event:
        response = await self.client.get(f"http://line-provider-fast-api:8080/event/{event_id}")  # Url to constant
        if response.status_code == status.HTTP_200_OK:
            return Event(**response.json())
        raise NotFoundException(message=f"Event with id: {event_id} is not found")
