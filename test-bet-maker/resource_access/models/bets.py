import decimal

from sqlalchemy import Column, String, Integer, CheckConstraint, Numeric, DECIMAL
from sqlalchemy.dialects.postgresql import ENUM

from resource_access.db_base_class import Base
from schemas.enums import BetStatus


class BetDB(Base):
    __tablename__ = "bets"
    amount = Column(Numeric(100, 2), nullable=False)
    coefficient = Column(Numeric(100, 2), nullable=False)
    event_id = Column(String, nullable=False)
    status = Column(ENUM(BetStatus), nullable=False, default=BetStatus.not_played)
    __table_args__ = (
        CheckConstraint(amount > 0, name='check_bet_amount_to_positive'),
        CheckConstraint(coefficient > 0, name='check_bet_coefficient_to_positive'),
    )
