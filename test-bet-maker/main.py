from fastapi import FastAPI
from fastapi.exceptions import RequestValidationError
from starlette.exceptions import HTTPException
from starlette.middleware.cors import CORSMiddleware

from api.errors.app_error import app_error_handler
from api.errors.http_error import http_error_handler
from api.errors.validation_error import http422_error_handler
from api.v1.api import v1_router
from core.config import settings
from utils.common_exceptions import AppException


def get_application() -> FastAPI:
    application = FastAPI()

    application.add_middleware(
        CORSMiddleware,
        allow_origins=settings.ALLOWED_HOSTS,
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )

    application.add_exception_handler(HTTPException, http_error_handler)
    application.add_exception_handler(RequestValidationError, http422_error_handler)
    application.add_exception_handler(AppException, app_error_handler)

    application.include_router(v1_router, prefix="/api/v1")

    return application


app = get_application()
