#!/bin/sh
echo "Waiting for postgres..."

while ! nc -z "${POSTGRES_HOST}" "${POSTGRES_PORT}"; do
  sleep 0.1
done
echo "PostgreSQL started"

echo "Migrate the Database at startup of project"
alembic upgrade head

echo "Running uvicorn"
uvicorn main:app --reload --host 0.0.0.0 --port 8000 --workers 3 --log-level=debug
