from typing import List
from resource_access.http_clientt_repositories.event_repositories import EventRepositories
from resource_access.repositories.bet_repositories import BetRepositories
from schemas.bet_schemas import Bet
from schemas.event_schemas import Event


class EventHttpEngines:

    @staticmethod
    async def get_events() -> List[Event]:
        repository = EventRepositories()
        return await repository.get_all_events()

    @staticmethod
    async def get_event(event_id: str) -> Event:
        repository = EventRepositories()
        return await repository.get_event(event_id)
