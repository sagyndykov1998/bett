from typing import List

from sqlalchemy.ext.asyncio import AsyncSession

from resource_access.repositories.bet_repositories import BetRepositories
from schemas.bet_schemas import Bet
from schemas.enums import BetStatus


class BetEngines:

    def __init__(self, session: AsyncSession):
        self.session = session

    async def list_bets(self) -> List[Bet]:
        repository = BetRepositories(session=self.session)
        return await repository.get_bets()

    async def creat_bet(self, new_bet: Bet) -> Bet:
        repository = BetRepositories(session=self.session)
        return await repository.create_bet(new_bet)

    async def update_bet_statuses_by_event_id(self, event_id: str, status: BetStatus):
        repository = BetRepositories(session=self.session)
        return await repository.update_not_played_bets_statuses_by_event_id(event_id, status)
