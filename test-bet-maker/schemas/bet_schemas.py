from typing import Optional

from pydantic import BaseModel, condecimal

from schemas.enums import BetStatus


class Bet(BaseModel):
    id: Optional[int] = None
    amount: Optional[float] = None
    coefficient: Optional[float] = None
    event_id: Optional[str] = None
    status: Optional[BetStatus] = None

    class Config:
        orm_mode = True


class BetOut(BaseModel):
    id: int
    amount: condecimal(decimal_places=2)
    coefficient: condecimal(decimal_places=2)
    event_id: str
    status: BetStatus

    class Config:
        orm_mode = True
        schema_extra = {
            "example": {
                "id": "1",
                "amount": 100,
                "coefficient": 2.1,
                "event_id": "event-1",
                "status": "not_played"
            }
        }


class BetIn(BaseModel):
    amount: condecimal(gt=0, decimal_places=2)
    event_id: str

    class Config:
        schema_extra = {
            "example": {
                "event_id": "1",
                "amount": 10.2,
            }
        }
