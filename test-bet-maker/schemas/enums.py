from enum import Enum


class BetStatus(str, Enum):
    not_played = 'not_played'
    win = 'win'
    loose = 'loose'
