import datetime
from typing import Optional

from pydantic import BaseModel


class Event(BaseModel):
    event_id: Optional[str] = None
    deadline: Optional[int] = None
    coefficient: Optional[float] = None
    state: Optional[str] = None

    class Config:
        orm_mode = True
