from starlette import status


class AppException(Exception):
    default_message = 'Something went wrong'
    default_code = status.HTTP_400_BAD_REQUEST

    def __init__(self, message=None, code=None, dev_message=None):
        self.message = message if message else self.default_message
        self.dev_message = dev_message if dev_message else self.default_message
        self.code = code if code else self.default_code


class EventServiceNotAvailableException(AppException):
    default_message = "Something wrong with event service"
    default_code = status.HTTP_503_SERVICE_UNAVAILABLE


class NotFoundException(AppException):
    default_message = "Object not found"
    default_code = status.HTTP_404_NOT_FOUND


class TimeOutException(AppException):
    default_message = "Event is closed"
    default_code = status.HTTP_400_BAD_REQUEST
