import time
from typing import List
from sqlalchemy.ext.asyncio import AsyncSession
from engines.bet_engines import BetEngines
from engines.event_engines import EventHttpEngines

from schemas.bet_schemas import Bet
from utils.common_exceptions import TimeOutException


async def create_bet_usecase(db_session: AsyncSession, new_bet: Bet):
    bet_engine = BetEngines(db_session)
    event_engine = EventHttpEngines()
    event = await event_engine.get_event(new_bet.event_id)
    if event.deadline < time.time():
        raise TimeOutException()
    new_bet.coefficient = event.coefficient
    return await bet_engine.creat_bet(new_bet)


async def list_all_bet_usecase(db_session: AsyncSession) -> List[Bet]:
    bet_engine = BetEngines(db_session)
    return await bet_engine.list_bets()
