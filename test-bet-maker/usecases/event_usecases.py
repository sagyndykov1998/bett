from sqlalchemy.ext.asyncio import AsyncSession

from engines.bet_engines import BetEngines
from engines.event_engines import EventHttpEngines
from schemas.enums import BetStatus
from schemas.event_schemas import Event


async def get_all_events_usecase():
    event_engine = EventHttpEngines()
    return await event_engine.get_events()


async def react_to_event_status_usecase(db_session: AsyncSession, event: Event):
    bet_engine = BetEngines(db_session)
    new_status = BetStatus.not_played
    if event.state == '3':
        new_status = BetStatus.loose
    elif event.state == '2':
        new_status = BetStatus.win
    await bet_engine.update_bet_statuses_by_event_id(event.event_id, new_status)
