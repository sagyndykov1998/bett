from typing import List


def remove_duplicates_from_list_of_dict(l: List):
    seen = set()
    d = len(l)
    while d!=0:
        t = tuple(l[d].items())
        print(d)
        if t not in seen:
            seen.add(t)
        else:
            l.pop(d)
    return l.reverse()


l = [{"key1": "value1"}, {"k1": "v1", "k2": "v2", "k3": "v3"}, {}, {}, {}, {"key1": "value1"}, {"key1": "value1"},
     {"key2": "value2"}]
print(remove_duplicates_from_list_of_dict(l))
print(l)
